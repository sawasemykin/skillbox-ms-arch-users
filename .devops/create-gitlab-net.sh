#!/bin/sh

docker network create -d bridge \
   --opt com.docker.network.bridge.enable_ip_masquerade="true" \
   --opt com.docker.network.bridge.enable_icc="true" \
   --opt com.docker.network.bridge.host_binding_ipv4="0.0.0.0" \
   --opt com.docker.network.driver.mtu="1500" \
   gitlab-net

docker network connect gitlab-net gitlab-runner
docker network connect gitlab-net sonarqube

#docker network disconnect bridge gitlab-runner
#docker network disconnect bridge sonarqube
