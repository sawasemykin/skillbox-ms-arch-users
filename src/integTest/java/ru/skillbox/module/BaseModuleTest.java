package ru.skillbox.module;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.io.InputStream;

@SpringBootTest
//@AutoConfigureMockMvc
@Testcontainers
@DirtiesContext
public class BaseModuleTest {

    private static final String POSTGRES_IMAGE = "postgres:12";

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @BeforeEach
    private void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Container
    public static PostgreSQLContainer<?> postgresDB = new PostgreSQLContainer<>(POSTGRES_IMAGE)
            .withDatabaseName("users")
            .withUsername("postgres")
            .withPassword("postgres");

    @DynamicPropertySource
    public static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresDB::getJdbcUrl);
        registry.add("spring.datasource.username", postgresDB::getUsername);
        registry.add("spring.datasource.password", postgresDB::getPassword);
    }

    public <T> T read(String fileName, Class<T> clazz) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream expectedResponseIS = classLoader.getResourceAsStream(fileName);
            return objectMapper.readValue(expectedResponseIS, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
