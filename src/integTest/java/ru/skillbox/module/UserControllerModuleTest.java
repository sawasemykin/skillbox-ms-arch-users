package ru.skillbox.module;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.skillbox.dto.UserDto;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UserControllerModuleTest extends BaseModuleTest {

    @Test
    @DisplayName("Модульный тест. Создание пользователя")
    @Sql("/data.sql")
    void createUser() throws Exception {
        UserDto user = read("user.json", UserDto.class);
        user.setId(null);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo(1)));
    }
}
