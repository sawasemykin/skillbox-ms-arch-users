package ru.skillbox.regress;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.skillbox.dto.UserDto;
import ru.skillbox.module.BaseModuleTest;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


class UserControllerRegressTest extends BaseModuleTest {

    private final static int USER_1_ID = 1;
    private final static int USER_2_ID = 2;

    private UserDto user1;
    private String responseUser1;
    private UserDto user2;
    private String responseUser2;

    @Test
    @DisplayName(
            """
            Регрессионный тест по сценарию:\s
                1. Создать двух пользователей.\s
                2. Успешно получить их по ID.\s
                3. Провести поиск пользователей.\s
                4. Подписать одного пользователя на другого.\s
                5. Проверить изменившиеся данные.\s
                6. Удалить подписку.\s
                7. Проверить изменившиеся данные.\s
                8. Проверить частичное изменение данных (PUT-запрос).\s
                9. Проверить изменившиеся данные.\s
                10. Удалить пользователей.\s
                11. Получить ошибку 404 по ID пользователей.
            """
    )
    @Sql("/data.sql")
    void doRegress() throws Exception {
        createUsers();
        getUserById();
        getAllUsers();
        subscribe();
        unsubscribe();
        updateUser();
        deleteUsers();
    }

    private void createUsers() throws Exception {
        user1 = read("user.json", UserDto.class);
        responseUser1 = objectMapper.writeValueAsString(user1);
        user1.setId(null);
        user2 = read("anotherUser.json", UserDto.class);
        responseUser2 = objectMapper.writeValueAsString(user2);
        user2.setId(null);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.equalTo(USER_1_ID)));

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.equalTo(USER_2_ID)));
    }

    private void getUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(content().json(responseUser1));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", USER_2_ID))
                .andExpect(status().isOk())
                .andExpect(content().json(responseUser2));
    }

    private void getAllUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));

    }

    private void subscribe() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users/{id}/subscriptions/{subscriptionId}", USER_1_ID, USER_2_ID))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscriptions", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].id", Matchers.equalTo(USER_2_ID)));
//                .andExpect(jsonPath("$").value(Lists.newArrayList(responseUser2)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscribers", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscriptions", USER_2_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscribers", USER_2_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].id", Matchers.equalTo(USER_1_ID)));
    }

    private void unsubscribe() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/{id}/subscriptions/{subscriptionId}", USER_1_ID, USER_2_ID))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscriptions", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscribers", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscriptions", USER_2_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}/subscribers", USER_2_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
    }

    private void updateUser() throws Exception {
        user1.setId((long) USER_1_ID);
        var birthdayUpdated = LocalDate.of(2000, 2, 28);
        user1.setBirthday(birthdayUpdated);
        var requestUser1Updated = objectMapper.writeValueAsString(user1);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/users/{id}", USER_1_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestUser1Updated))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.equalTo(USER_1_ID)));

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.equalTo(USER_1_ID)))
                .andExpect(jsonPath("$.birthday", Matchers.equalTo(birthdayUpdated.toString())));

    }

    private void deleteUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/{id}", USER_1_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.equalTo(USER_1_ID)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/{id}", USER_2_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.equalTo(USER_2_ID)));
        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", Matchers.hasSize(0)));

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", USER_1_ID)).andExpect(status().isNotFound());
        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", USER_2_ID)).andExpect(status().isNotFound());

    }
}
