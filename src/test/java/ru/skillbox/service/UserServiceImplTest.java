package ru.skillbox.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.skillbox.dto.UserDto;
import ru.skillbox.mapper.*;
import ru.skillbox.model.User;
import ru.skillbox.repository.UserRepository;
import ru.skillbox.service.UserService;
import ru.skillbox.service.UserServiceImpl;
import ru.skillbox.transaction.TransactionManagerSpring;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;

@SpringBootTest(classes = {
        AvatarMapperImpl.class,
        AvatarSetMapperImpl.class,
        CityMapperImpl.class,
        HardSkillMapperImpl.class,
        HardSkillSetMapperImpl.class,
        UserMapperImpl.class,
        UserListMapperImpl.class,
        TransactionManagerSpring.class,
        UserServiceImpl.class
})
class UserServiceImplTest {
    @Autowired
    UserService userService;
    @MockBean
    UserRepository userRepository;

    private final ObjectMapper objectMapper;

    {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    @DisplayName("Подписка/отписка")
    void subscribeUnsubscribeTest() {
        User user = read("user.json", User.class);
        User subscription = read("subscription.json", User.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findById(subscription.getId())).thenReturn(Optional.of(subscription));

        userService.subscribe(user.getId(), subscription.getId());

        Assertions.assertThat(user.getSubscriptions()).hasSize(1).allMatch(subscription::equals);
        Assertions.assertThat(user.getSubscribers()).isEmpty();
        Assertions.assertThat(subscription.getSubscriptions()).isEmpty();
        Assertions.assertThat(subscription.getSubscribers()).hasSize(1).allMatch(user::equals);

        Mockito.verify(userRepository).findById(user.getId());
        Mockito.verify(userRepository).findById(subscription.getId());

        userService.unsubscribe(user.getId(), subscription.getId());

        Assertions.assertThat(user.getSubscriptions()).isEmpty();
        Assertions.assertThat(user.getSubscribers()).isEmpty();
        Assertions.assertThat(subscription.getSubscriptions()).isEmpty();
        Assertions.assertThat(subscription.getSubscribers()).isEmpty();

        Mockito.verify(userRepository, times(2)).findById(user.getId());
        Mockito.verify(userRepository, times(2)).findById(subscription.getId());
    }

    @Test
    @DisplayName("Получение подписок/подписчиков")
    void getSubscriptionsSubscribersTest() {
        User user = read("user.json", User.class);
        User subscription = read("subscription.json", User.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findById(subscription.getId())).thenReturn(Optional.of(subscription));

        userService.subscribe(user.getId(), subscription.getId());
        List<UserDto> subscriptions = userService.getSubscriptions(user.getId());
        List<UserDto> subscribers = userService.getSubscribers(subscription.getId());

        Assertions.assertThat(subscriptions).isNotNull().hasSize(1).allMatch(dto -> subscription.getId().equals(dto.getId()));
        Assertions.assertThat(subscribers).isNotNull().hasSize(1).allMatch(dto -> user.getId().equals(dto.getId()));

        Mockito.verify(userRepository, times(2)).findById(user.getId());
        Mockito.verify(userRepository, times(2)).findById(subscription.getId());
    }

    private <T> T read(String fileName, Class<T> clazz) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream expectedResponseIS = classLoader.getResourceAsStream(fileName);
            return objectMapper.readValue(expectedResponseIS, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
