package ru.skillbox.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.skillbox.controller.UserController;
import ru.skillbox.dto.UserDto;
import ru.skillbox.service.UserService;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Создание пользователя. Успех")
    void createUser() throws Exception {
        UserDto user = createUserDto();
        user.setId(null);
        Mockito.when(userService.createUser(user)).thenReturn(1L);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)));

        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo(1)));

        Mockito.verify(userService).createUser(user);
    }

    private UserDto createUserDto() {
        UserDto userDto = new UserDto();
        userDto.setId(1L);
        userDto.setFirstName("Vasya");
        userDto.setLastName("Petrov");
        return userDto;
    }

    @Test
    @DisplayName("Создание пользователя. Невалидный запрос")
    void createUserFailed() throws Exception {
        UserDto user = createUserDto();
        user.setId(null);
        Mockito.when(userService.createUser(user)).thenThrow(DataIntegrityViolationException.class);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)));

        resultActions.andExpect(MockMvcResultMatchers.status().is5xxServerError());

        Mockito.verify(userService).createUser(user);
    }

    @Test
    @DisplayName("Обновление пользователя")
    void updateUser() throws Exception {
        UserDto user = createUserDto();
        Mockito.when(userService.updateUser(user, user.getId())).thenReturn(user.getId());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put("/users/{id}", user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)));

        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo(1)));

        Mockito.verify(userService).updateUser(user, user.getId());
    }



    @Test
    @DisplayName("Удаление пользователя")
    void deleteUser() throws Exception {
        final long userId = 1;
        Mockito.when(userService.deleteUser(userId)).thenReturn(userId);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/{id}", userId));

        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo(1)));

        Mockito.verify(userService).deleteUser(userId);
    }

    @Test
    @DisplayName("Получение пользователя по id")
    void getUser() throws Exception {
        UserDto user = createUserDto();

        Mockito.when(userService.getUser(user.getId())).thenReturn(user);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/{id}", user.getId()));

        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().json(objectMapper.writeValueAsString(user)));

        Mockito.verify(userService).getUser(user.getId());
    }

    @Test
    @DisplayName("Получение всех пользователей")
    void getUsersTest() throws Exception {
        UserDto userDto = createUserDto();
        List<UserDto> userDtoList = List.of(userDto);

        Mockito.when(userService.getUsers()).thenReturn(userDtoList);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users"));

        resultActions
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().json(objectMapper.writeValueAsString(userDtoList)));

        Mockito.verify(userService).getUsers();
    }

    @Test
    @DisplayName("Подписка")
    void subscribeTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users/{id}/subscriptions/{subscriptionId}", 1L, 2L));

        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        Mockito.verify(userService).subscribe(1L, 2L);
    }

    @Test
    @DisplayName("Отписка")
    void unsubscribeTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/{id}/subscriptions/{subscriptionId}", 1L, 2L));

        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        Mockito.verify(userService).unsubscribe(1L, 2L);
    }

    @Test
    @DisplayName("Получение подписок")
    void getSubscriptionsTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/{id}/subscriptions", 1L));

        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        Mockito.verify(userService).getSubscriptions(1L);
    }

    @Test
    @DisplayName("Получение подписчиков")
    void getSubscribersTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/{id}/subscribers", 1L));

        resultActions.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        Mockito.verify(userService).getSubscribers(1L);
    }
}
