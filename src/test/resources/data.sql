INSERT INTO users_schema.cities (id, name) VALUES (1, 'Moscow');
INSERT INTO users_schema.cities (id, name) VALUES (2, 'Saint Petersburg');

INSERT INTO users_schema.hard_skills (id, name) VALUES (1, 'java');
INSERT INTO users_schema.hard_skills (id, name) VALUES (2, 'spring');
INSERT INTO users_schema.hard_skills (id, name) VALUES (3, 'db');
INSERT INTO users_schema.hard_skills (id, name) VALUES (4, 'js');
INSERT INTO users_schema.hard_skills (id, name) VALUES (5, 'react');