package ru.skillbox.transaction;

public interface TransactionManager {

    <T> T doInTransaction(TransactionAction<T> action);
}
