## Блок 2
### Testing<br>
`./gradlew clean build`<br>
`./gradlew clean build -x check` - сборка без тестов<br>
`./gradlew clean test` - запуск юнит-тестов<br>
`./gradlew clean integrationTest` - запуск модульных тестов<br>

### Helm
Используемые репозитории
- `helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/`
- `helm repo add nginx-stable https://helm.nginx.com/stable`
- `helm repo add bitnami https://charts.bitnami.com/bitnami`
- `helm repo add runix https://helm.runix.net`
- `helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube`

Раскатка сервисов из чартов
- `helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard`
- `helm upgrade --install install nginx-ingress nginx-stable/nginx-ingress`
- 
- `helm upgrade --install -n sonarqube sonarqube sonarqube/sonarqube`
- 
- `helm upgrade --install users-db --namespace default -f .chart/users-db/values.yaml bitnami/postgresql`
- `helm upgrade --install pgadmin --namespace default runix/pgadmin4`
- 
- `helm upgrade --install users-api .chart/users-api/`

Пробрасывание портов для работы с подами